/*********************************************************

Author: Christopher Rains
Date Created: 31 October 2011
Date Last Modified: 22 July 2013

*********************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>


int main(int argc, char *argv[])
{
    int sock;                       /* Socket */
    struct sockaddr_in ServAddr; 	/* Local address */
    struct sockaddr_in ClntAddr; 	/* Client address */
    unsigned int cliAddrLen;        /* Length of incoming message */
    char echoBuffer[2000];        	/* Buffer for echo string */
    unsigned short PortNo;     		/* Server port */
    int newsock;
    int numread;
	char name[20];

	int MAX_SIZE = 160;
	int RETURN_SIZE = 225;
	char sendbuffer[225];
	char readbuffer[2000];
	char text[200];

	printf("Enter your name: ");
	gets(name);
	strcat(name, ": "); 

	// Test for correct number of parameters
    if (argc != 2) {
        printf("Usage:  %s <SERVER PORT>\n", argv[0]);
        return (1);
    }

	// First arg:  local port
    PortNo = atoi(argv[1]);

    // Create socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            printf("socket() failed");
            return (1);
    }

    // Construct local address structure
    memset(&ServAddr, 0, sizeof(ServAddr));   		/* Zero out structure */
    ServAddr.sin_family = AF_INET;                	/* Internet address family */
    ServAddr.sin_addr.s_addr = htonl(INADDR_ANY); 	/* Any incoming interface */
    ServAddr.sin_port = htons(PortNo);      		/* Local port */

    // Bind to the local address
    printf("About to bind to port %d\n", PortNo);    
    if (bind(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
        printf ("bind() failed");
        return (1);
    }

	for(;;) {
		listen(sock, 5);	
	
		numread = 1;

		// Set the size of the in-out parameter
        cliAddrLen = sizeof(ClntAddr);

        // wait for connection
        newsock = accept(sock, (struct sockaddr *) &ClntAddr, &cliAddrLen);
	
		// Run forever
		while (numread != 0) {
			numread = read(newsock, readbuffer, RETURN_SIZE);
			if(numread == 0) {
				break;
			} else {	
				printf("\t\t\t%s\n", readbuffer);
			} 
			printf("%s", name);
			gets(text);

			strcpy(sendbuffer, name);
			strcat(sendbuffer, text);
			write(newsock, &sendbuffer, strlen(sendbuffer)+1);
		}

		close(newsock);
		printf("\n\nUser Offline. Waiting for new user.\n\n");
	}
}

