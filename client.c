/*********************************************************

Author: Christopher Rains
Date Created: 31 October 2011
Date Last Modified: 22 July 2013

*********************************************************/
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int sock;                         
    struct sockaddr_in ServAddr;     
    unsigned short ServPort;         

    if (argc != 3) {
        printf("Usage: %s <ServerAddress> <ServerPortNumber>\n", argv[0]);
        return(1);
    }
    
    ServPort = atoi(argv[2]);  
    
    memset(&ServAddr, 0, sizeof(ServAddr));    
    ServAddr.sin_family = AF_INET;                 
    ServAddr.sin_addr.s_addr = inet_addr(argv[1]);  
    ServAddr.sin_port = htons(ServPort);     

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Socket() Failed!\n");
        return(1);
    }
  
    if( connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
        printf("Connect() Failed!\n");
        return(1);
    }

	char name[20];

	int MAX_SIZE = 160;
	int RETURN_SIZE = 225;
	char sendbuffer[225];
	char readbuffer[225];
	char text[200];
	int numread = 1;

	printf("Enter your name: ");
	gets(name);
	strcat(name, ": ");

	while(strcmp(text, "#") != 0) {
		printf("%s", name);
		gets(text);
		if(strcmp(text, "#") != 0) {
			if(strlen(text) < MAX_SIZE) {
				strcpy(sendbuffer, name);
				strcat(sendbuffer, text);		
				write(sock, &sendbuffer, strlen(sendbuffer)+1);
				numread = read(sock, readbuffer, RETURN_SIZE);
				if(numread == 0) {
					printf("Error: Socket Terminated!\n");
					strcpy(text, "#");
				}
				else {	
					printf("\t\t\t%s\n", readbuffer);
				} 
			}
			else {
				printf("Error: Max msg size exceeded!\n");
			}
		}	
	}
  
    close(sock);
}


